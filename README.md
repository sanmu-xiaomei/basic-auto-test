# 自动化测试框架

- 使用技术 Python+Selenium3+Request+Allure2+Pytest+Jenkins+Pymysql+openpyxl
- 通用型框架已经在自己公司成功落地使用!
- 如果这个项目对你有帮助或者有启发的话麻烦多多点赞哦！
  - [![XRdTFH.jpg](https://s1.ax1x.com/2022/06/13/XRdTFH.jpg)](https://imgtu.com/i/XRdTFH)
- conftest.py 封装公共的driver对象实现调用driver对象自动识别本地chrome版本自动下载driver，免除自己手动下载匹配的问题。代码适配无头浏览器自动识别是否是linux，从而使用无头浏览器和selenium grid
##### 可以进行：
1. 接口自动化测试通过openpyxl进行数据驱动
2. UI自动化测试使用PageObjectModel模式进行
3. 集成jenkins后自动通过钉钉发送详细allure报告+简要内容报告。
   1. ![avatar](https://i.postimg.cc/RZKm9SqG/image.jpg)
4. 集成钉钉机器人可以针对场景进行钉钉通知。
#### 安装教程
1.  拉取项目安装requirements.txt中的包
2.  修改ConfigFile/config.yaml配置文件
3.  编写脚本执行
#### 备注
1. 不用自己安装chromedriver 会自动读取浏览器版本进行安装
#### 项目目录介绍
1. Commons --> 存放公共方法
   1. basic_request --> 请求接口 钉钉 dubbo（暂未实现）
   2. operation_file --> 操作yaml excel
   3. ui_auto --> ui自动化的basicPage 解析allure测试报告
   4. util --> 工具类 json解析 数据库操作 日志记录 等
   5. api_auto --> 接口自动化工具类
2. ConfigFile --> 项目的配置文件
3. PageObject --> po文件
4. TestCase --> 测试用例
5. TestData --> 测试数据
   1. TestCase.xlsx --> 不同的sheet存放不同的api文件
   2. SchemaData --> 存放校验接口字段的yaml文件
#### 更新日志
- 20220619
  1. 接口自动化支持多关键字校验按照下图在excel中维护用例（写一个参数就换行）
  2. Commons/api_auto/assert_method.py 支持接口多字段校验
     1. [![Xj28pV.jpg](https://s1.ax1x.com/2022/06/19/Xj28pV.jpg)](https://imgtu.com/i/Xj28pV)
  


